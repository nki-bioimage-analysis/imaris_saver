"""
Excersise for how to write a simple commandline program.

Copyright 2022 R.Harkes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from datetime import datetime

from PyImarisWriter import PyImarisWriter as PW


class MyCallbackClass(PW.CallbackClass):
    def __init__(self):
        self.mUserDataProgress = 0

    def RecordProgress(self, progress, total_bytes_written):
        progress100 = int(progress * 100)
        if progress100 - self.mUserDataProgress >= 5:
            self.mUserDataProgress = progress100
            print(
                f"Progress {self.mUserDataProgress}, GBytes written: {total_bytes_written / 2 ** 30 :.2f}"
            )


def pyimarissaver(f, file_out, save_level):
    image_size = PW.ImageSize(
        x=f.reslevelshapes[save_level][2],
        y=f.reslevelshapes[save_level][1],
        z=f.reslevelshapes[save_level][0],
        c=len(f.channels),
        t=len(f.timepoints),
    )
    dimension_sequence = PW.DimensionSequence("x", "y", "z", "c", "t")
    block_size = PW.ImageSize(
        x=f.reslevelshapes[save_level][2],
        y=f.reslevelshapes[save_level][1],
        z=f.reslevelshapes[save_level][0],
        c=1,
        t=1,
    )
    sample_size = PW.ImageSize(x=1, y=1, z=1, c=1, t=1)
    output_filename = str(file_out)
    options = PW.Options()
    options.mNumberOfThreads = 12
    options.mCompressionAlgorithmType = PW.eCompressionAlgorithmGzipLevel2
    options.mEnableLogProgress = True
    application_name = "Imaris-Saver"
    application_version = "1.1.1"

    converter = PW.ImageConverter(
        "uint16",
        image_size,
        sample_size,
        dimension_sequence,
        block_size,
        output_filename,
        options,
        application_name,
        application_version,
        MyCallbackClass(),
    )
    num_blocks = image_size / block_size
    block_index = PW.ImageSize()
    for c in range(num_blocks.c):
        block_index.c = c
        for t in range(num_blocks.t):
            block_index.t = t
            dset = f.get(save_level, t, c)[:]  # READ WHOLE CHANNEL AT ONCE (x,y,z)
            if not dset.flags["C_CONTIGUOUS"]:
                print("WARNING: NOT C_TONTIGUOUS")
                dset = dset.copy("c")
            for z in range(num_blocks.z):
                block_index.z = z
                for y in range(num_blocks.y):
                    block_index.y = y
                    for x in range(num_blocks.x):
                        block_index.x = x
                        if converter.NeedCopyBlock(block_index):
                            converter.CopyBlock(dset, block_index)
    adjust_color_range = False
    image_extents = PW.ImageExtents(*f.get_image_extends())
    parameters = PW.Parameters()
    for i in range(image_size.c):
        parameters.set_channel_name(i, f.get_channel_name(i))
    time_infos = [datetime.today()]
    color_infos = [PW.ColorInfo() for _ in range(image_size.c)]
    for i in range(image_size.c):
        color_infos[i].set_base_color(PW.Color(*f.get_channel_rgba(i)))
        (cr_min, cr_max) = f.get_colorrange(i)
        color_infos[i].mRangeMin = cr_min
        color_infos[i].mRangeMax = cr_max
        color_infos[i].mGammaCorrection = f.get_gamma(i)
    converter.Finish(
        image_extents, parameters, time_infos, color_infos, adjust_color_range
    )
    converter.Destroy()
