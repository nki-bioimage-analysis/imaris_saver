"""
Excersise for how to write a simple commandline program.
Open an imaris file with h5py, and write a slice of it with PyImarisWriter.

Copyright 2022 R.Harkes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from pathlib import Path
from pyimarisreader import PyImarisReader
from PyImarisWriter import PyImarisWriter as PW
from pyimarissaver import pyimarissaver


class MyCallbackClass(PW.CallbackClass):
    def __init__(self):
        self.mUserDataProgress = 0

    def RecordProgress(self, progress, total_bytes_written):
        progress100 = int(progress * 100)
        if progress100 - self.mUserDataProgress >= 5:
            self.mUserDataProgress = progress100
            print(f"Progress {self.mUserDataProgress}, GBytes written: {total_bytes_written / 2 ** 30 :.2f}")


try:
    files = list(Path.cwd().glob('*.ims'))
    if files:
        if len(files) == 1:
            file = files[0]
        else:
            for i, file in enumerate(files):
                print(f"{i} : {file}")
            file_in = int(input(f"Choose file (0-{len(files) - 1}) : "))
            file = files[file_in]
        print(f"Reading {file}...")
        with PyImarisReader(file) as ims:
            for i, level in enumerate(ims.reslevels):
                pixsz = " ".join([f"{x:.2f}" for x in ims.pixelsizes[i]])
                print(f"{i} : {level} {ims.reslevelshapes[i]}pix ({pixsz})um/pix")
            reslevel_in = int(input(f"Choose resolutionlevel (0-{len(ims.reslevels) - 1}) or -1 to cancel : "))
            if reslevel_in > -1:
                newfile = Path(Path.cwd(), file.stem + '(res' + str(reslevel_in) + ').ims')
                print(f"Saving {ims.reslevelshapes[reslevel_in]} pixels as {newfile}")
                pyimarissaver(ims, newfile, reslevel_in)
        print("FINISHED!")
    else:
        print("No .ims files found!")
except Exception as e:
    print(e)
finally:
    input('Press ENTER to exit')
