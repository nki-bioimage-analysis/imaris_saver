"""
Excersise for how to write a simple commandline program.

Copyright 2022 R.Harkes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from pathlib import Path
import numpy as np
import h5py


class PyImarisReader:
    """
    Class to help with reading imaris files
    """

    def __init__(self, filename):
        self.filename = Path(filename)
        self.f = h5py.File(filename, "r")
        self.fileattributes = attrs2dict(self.f.attrs)
        self.reslevels = list(self.f["DataSet"].keys())
        self.timepoints = list(self.f["DataSet"][self.reslevels[0]].keys())
        self.channels = list(
            self.f["DataSet"][self.reslevels[0]][self.timepoints[0]].keys()
        )
        self.reslevelshapes_raw = [
            self.get_raw(i, 0, 0).shape for i in range(len(self.reslevels))
        ]
        self.reslevelshapes = [
            self.get_imsz(i, 0, 0) for i in range(len(self.reslevels))
        ]
        self.pixelsizes = [self.get_pixsz(i, 0, 0) for i in range(len(self.reslevels))]

    def get_imsz(self, res, time, ch):
        dat_grp = self.f["DataSet"][self.reslevels[res]][self.timepoints[time]][self.channels[ch]]
        imagesizex = int(dat_grp.attrs.get("ImageSizeX").tobytes().decode())
        imagesizey = int(dat_grp.attrs.get("ImageSizeY").tobytes().decode())
        imagesizez = int(dat_grp.attrs.get("ImageSizeZ").tobytes().decode())
        return imagesizez, imagesizey, imagesizex

    def get(self, res, time, ch):
        dat = self.get_raw(res, time, ch)
        imsz = self.get_imsz(res, time, ch)
        return dat[0: imsz[0], 0: imsz[1], 0: imsz[2]]

    def get_pixsz(self, res, time, ch):
        imagesizez, imagesizey, imagesizex = self.get_imsz(res, time, ch)
        min_x, min_y, min_z, max_x, max_y, max_z = self.get_image_extends()
        pxszx = (max_x - min_x) / imagesizex
        pxszy = (max_y - min_y) / imagesizey
        pxszz = (max_z - min_z) / imagesizez
        return pxszz, pxszy, pxszx

    def get_compression(self, res, time, ch):
        dat = self.get_raw(res, time, ch)
        return f"{dat.compression} ; {dat.compression_opts}"

    def get_raw(self, res, time, ch):
        return self.f['DataSet'][self.reslevels[res]][self.timepoints[time]][self.channels[ch]]['Data']

    def printreslevels(self):
        for i, reslevel in enumerate(self.reslevels):
            print(f"{reslevel} is {self.reslevelshapes[i]}")

    def get_channel_name(self, ch):
        return self.f['DataSetInfo'][self.channels[ch]].attrs.get('Name').tobytes().decode()

    def get_channel_rgba(self, ch):
        color = self.f['DataSetInfo'][self.channels[ch]].attrs.get('Color').tobytes().decode()
        color = [float(x) for x in color.split(' ')]
        color_opacity = self.f['DataSetInfo'][self.channels[ch]].attrs.get('ColorOpacity').tobytes().decode()
        color_opacity = float(color_opacity)
        return *color, color_opacity

    def get_colorrange(self, ch):
        cr = self.f['DataSetInfo'][self.channels[ch]].attrs.get('ColorRange').tobytes().decode()
        cr = cr.split()
        return float(cr[0]), float(cr[1])

    def get_gamma(self, ch):
        return float(self.f['DataSetInfo'][self.channels[ch]].attrs.get('GammaCorrection').tobytes().decode())

    def get_image_extends(self):
        """
        Note that the minimum and maximum of the bounding box lie on the border of the border voxels, not in the
        center of the border voxels. This means that an image consisting of a single slice in Z will have ExtMax2 > ExtMin2..
        :return:
        """
        min_x = float(self.f['DataSetInfo']['Image'].attrs['ExtMin0'].tobytes().decode())
        min_y = float(self.f['DataSetInfo']['Image'].attrs['ExtMin1'].tobytes().decode())
        min_z = float(self.f['DataSetInfo']['Image'].attrs['ExtMin2'].tobytes().decode())
        max_x = float(self.f['DataSetInfo']['Image'].attrs['ExtMax0'].tobytes().decode())
        max_y = float(self.f['DataSetInfo']['Image'].attrs['ExtMax1'].tobytes().decode())
        max_z = float(self.f['DataSetInfo']['Image'].attrs['ExtMax2'].tobytes().decode())
        return min_x, min_y, min_z, max_x, max_y, max_z

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, traceback):
        self.f.close()


def attrs2dict(attrs):
    """ Convert attributes of HDF5 to a python dictionary"""
    d = {}
    for a in attrs:
        if type(attrs[a][0]) == np.bytes_:
            d[a] = attrs[a].tobytes().decode()
        else:
            if attrs[a].size == 1:
                d[a] = attrs[a][0]
            else:
                d[a] = attrs[a]
    return d
