"""
compare two ims files to see why there is a hyperslab error with one
"""
from pathlib import Path
from pyimarisreader import PyImarisReader

pth = Path(Path.cwd(), 'test')
files = [PyImarisReader(x) for x in Path(pth).glob('*.ims')]
print('----compression---')
for file in files:
    print(f"{file.filename.name} ; {file.get_compression(0, 0, 0)}")

print('----top resolution---')
for file in files:
    print(f"{file.filename.name} ; {file.reslevelshapes[0]}")



