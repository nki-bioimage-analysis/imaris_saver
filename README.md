# Imaris Saver
An excersise to write a simple commandline program that does this:
* Ask the user to specify a .ims file.
* Display properties of resolution levels in the file.
* Ask the user what resolution level should be saved.
* Save that resolution level as new .ims file.

## Build instructions
`bpImarisWriter96.dll` can be found here `\envs\imaris_saver\Lib\site-packages\PyImarisWriter\` and must be copied to the root of the project before building. It seems PyImarisWriter didn't supply hooks or something.

Build with: `pyinstaller imaris_saver.spec`